Developer Module that assists with generate barcodes in varies modes.
Like the below types of barcode are generating by this module.

std25
int25
ean8
ean13
code11
code39
code93
code128
codabar
msi
datamatrix

Coder Sniffer
-------------

See the README.txt file in the coder_sniffer directory.


Installation
------------

Copy coder.module to your module directory and then enable
on the admin modules page.
Enable the modules and the "barcode" menu callback
will create to do the test mode.


Automated Testing (PHPUnit)
---------------------------

Coder Sniffer comes with a PHPUnit test suite to make sure the sniffs
work correctly.
Use Composer to install the dependencies:

  composer install

Then execute the tests:

  ./vendor/bin/phpunit


Author
------
Ramasubbu
m.n.ramasubbu@gmail.com
